# mate
MATE, a Unified Model for Communication-Tolerant Scientific Applications

Publications:

+ [2019] Martin S.M., Baden S.B. [MATE, a Unified Model for Communication-Tolerant Scientific Applications.](https://link.springer.com/chapter/10.1007/978-3-030-34627-0_10) In: Hall M., Sundar H. (eds) Languages and Compilers for Parallel Computing. LCPC 2018. Lecture Notes in Computer Science, vol 11882. Springer, Cham

+ [2018] Martin, S. M. [MATE, a Unified Model for Communication-Tolerant Scientific Applications](https://escholarship.org/uc/item/4tz5v6r6) (Doctoral dissertation, UC San Diego).

+ [2017] Martin, S. M., Berger, M. J., & Baden, S. B. [Toucan—A Translator for Communication Tolerant MPI Applications](https://ieeexplore.ieee.org/abstract/document/7967190). In 2017 IEEE International Parallel and Distributed Processing Symposium (IPDPS) (pp. 998-1007). IEEE.

For more details, please visit: [http://mate.ucsd.edu/](http://mate.ucsd.edu/)
