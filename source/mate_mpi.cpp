#include "mate_runtime.h"
#include "mate_comm.h"
#include "mate_rank.h"
#include <mpi.h>
#include <string.h>

using namespace Mate;

int _mate_max_tag;
int _mate_max_localtasks;
Lock _MPILock;
map<int, int> MateMPITagReallocationMap;
int currentReallocationTag;

void initializeMateTags()
{
	// Setting Maximum Local Tasks for MATE
	_mate_max_localtasks = 1 << MAXLOCALTASKS;
	//printf("[MATE] Maximum MATE Local Tasks: %d\n", _mate_max_localtasks);

	// Setting Maximum MATE tag
	int *tag_max_ptr;	int flag;
	MPI_Comm_get_attr(MPI_COMM_WORLD, MPI_TAG_UB, &tag_max_ptr, &flag);
//	printf("[MATE] MPI Maximum Tag: %d\n",  *tag_max_ptr);

	_mate_max_tag = *tag_max_ptr / (_mate_max_localtasks*_mate_max_localtasks);
	//printf("[MATE] MATE Maximum Tag: %d\n", _mate_max_tag);

  // Verifying that local task count does not exceed maximum TAG when using MPI as backend
	if (_runtime->_local_rank_count > _mate_max_localtasks)
  {
  	if (_runtime->_process_id == 0)
    {
  	  printf("[MATE] Error: Virtualization Factor exceeds limit.\n");
  	  printf("[MATE] MPI Processes: %d\n", _runtime->_process_count);
  	  printf("[MATE] Mate Tasks:    %d\n", _runtime->_global_rank_count);
  	  printf("[MATE] Mate Tasks per MPI process requested: %d - Limit: %d\n", _runtime->_local_rank_count, _mate_max_localtasks);
  	}

  	 MPI_Finalize(); exit(0);
  }
}
