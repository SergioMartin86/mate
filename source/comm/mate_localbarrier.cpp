#include "mate_runtime.h"
#include "mate_comm.h"
#include "mate_rank.h"
#include <mpi.h>

using namespace Mate;

int _barrierCount;
bool _isGlobalBarrier;

// Mate_LocalBarrier is an auxiliar function to coordinate all tasks within a MPI process. The last task to reach rendezvous frees all the other tasks.
int Mate_LocalBarrier()
{
	Mate_Rank* currentTask = getCurrentRank();
	Worker* currentWorker = getCurrentWorker();

 //printf("Worker %d - Task %d - Arrived at Barrier\n", currentWorker->_localId, currentTask->_localId );

  bool allBarrier = false;
  _runtime->barrierLock.lock();
  currentTask->_isBarrier = true;
  _barrierCount++;
  if (_barrierCount == _runtime->_local_rank_count) allBarrier = true;
  _runtime->barrierLock.unlock();

 if (allBarrier == false)	 currentTask->yield();

 if (allBarrier == true)
	{
		if (_isGlobalBarrier == true)
		{
			_MPILock.lock();
			double t_init;
			if(_runtime->_useMateTiming) t_init = -MPI_Wtime();

			MPI_Barrier(MPI_COMM_WORLD);

			if(_runtime->_useMateTiming) currentTask->_barrierTime += t_init + MPI_Wtime();
			_MPILock.unlock();
		}

		_isGlobalBarrier = false;
		_barrierCount = 0;

		for (int i = 0; i < _runtime->_local_rank_count; i++) if (currentTask != &_runtime->_ranks[i]) _runtime->_ranksLock[i].lock();
		for (int i = 0; i < _runtime->_local_rank_count; i++) if (currentTask != &_runtime->_ranks[i]) _runtime->_ranks[i]._isReady = true;
		for (int i = 0; i < _runtime->_local_rank_count; i++) _runtime->_ranks[i]._isBarrier = false;
		for (int i = 0; i < _runtime->_local_rank_count; i++) if (currentTask != &_runtime->_ranks[i]) _runtime->_ranksLock[i].unlock();
	}

 return 0;
}

extern "C" void mate_localbarrier_(){ Mate_LocalBarrier(); }
