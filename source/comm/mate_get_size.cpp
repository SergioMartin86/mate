#include "mate_runtime.h"
#include "mate_comm.h"
#include "mate_rank.h"
#include <mpi.h>

using namespace Mate;

// This is a helper function that returns the actual MPI datatype size for the current node's architecture.
unsigned long int Mate_Get_Size(int count, MPI_Datatype datatype)
{
	int typeSize;
	_MPILock.lock();
	MPI_Type_size(datatype, &typeSize);
	_MPILock.unlock();
	return count*typeSize;
}
