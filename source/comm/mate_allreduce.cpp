#include "mate_runtime.h"
#include "mate_comm.h"
#include "mate_rank.h"
#include <cstring>
#include <mpi.h>

using namespace Mate;

void* _allreduce_accumulator;
void* _local_allreduce_accumulator;

int Mate_Allreduce(const void *sendbuf, void *recvbuf, int count, MPI_Datatype datatype, MPI_Op op, MPI_Comm comm)
{
	Mate_Rank* currentTask = getCurrentRank();

  Mate_LocalBarrier();

	unsigned long int _vectorSize = Mate_Get_Size(count, datatype);

  Mate_LocalBarrier();
  if (currentTask->_localId == 0) memcpy(_local_allreduce_accumulator, sendbuf, _vectorSize);
  Mate_LocalBarrier();
  if (currentTask->_localId != 0) Mate_OpExec(_local_allreduce_accumulator, sendbuf, count, datatype, op);
  Mate_LocalBarrier();

  if (currentTask->_localId == 0)
  {
		_MPILock.lock();
		int ret = MPI_Allreduce(_local_allreduce_accumulator, _allreduce_accumulator, count, datatype, op, comm);
		_MPILock.unlock();
		if (ret != MPI_SUCCESS) {fprintf(stderr, "[MATE] Error: MPI_Allreduce Failed\n"); MPI_Abort(MPI_COMM_WORLD, -1);}
  }

  Mate_LocalBarrier();
  memcpy(recvbuf, _allreduce_accumulator, _vectorSize);
  Mate_LocalBarrier();

  return MPI_SUCCESS;
}

extern "C" int mate_allreduce_(const void *sendbuf, void *recvbuf, int* count, MPI_Datatype* datatype, MPI_Op* op, MPI_Comm* comm) { return Mate_Allreduce(sendbuf, recvbuf, *count, *datatype, *op, *comm); }
