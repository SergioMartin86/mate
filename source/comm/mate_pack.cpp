#include "mate_runtime.h"
#include "mate_comm.h"
#include "mate_rank.h"
#include <mpi.h>

using namespace Mate;


int Mate_Pack(const void* source, int count, MPI_Datatype mpiType, void* dest, int size, int* position, MPI_Comm comm)
{
	int ret = MPI_Pack(source, count, mpiType, dest, size, position, comm);
	if (ret != MPI_SUCCESS) {fprintf(stderr, "[MATE] Error: MPI_Pack Failed\n"); MPI_Abort(MPI_COMM_WORLD, -1);}

  return ret;
}
