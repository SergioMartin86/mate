#include "mate_runtime.h"
#include "mate_comm.h"
#include "mate_rank.h"
#include <cstring>
#include <mpi.h>

using namespace Mate;

void* _bcast_buffer;

int Mate_Bcast( void *buffer, int count, MPI_Datatype datatype, int root, MPI_Comm comm)
{
	Mate_Rank* currentTask = getCurrentRank();
  int _localRoot;
  if (Mate_isLocalProcessRank(root, currentTask->_globalId)) _localRoot = _runtime->_globalToLocalRankIdMap[root];
  else																				            	 _localRoot = 0;

  Mate_LocalBarrier();

  if (currentTask->_localId == _localRoot)
  {
  	_bcast_buffer = buffer;
	  int mate_root = _runtime->_global_rank_map[root];

    _MPILock.lock();
    int ret = MPI_Bcast(buffer, count, datatype, mate_root, MPI_COMM_WORLD);
    _MPILock.unlock();
	  if (ret != MPI_SUCCESS) {fprintf(stderr, "[MATE] Error: MPI_Bcast Failed\n"); MPI_Abort(MPI_COMM_WORLD, -1);}
  }

  Mate_LocalBarrier();
  unsigned long int _vectorSize = Mate_Get_Size(count, datatype);
  if (currentTask->_localId != _localRoot) memcpy(buffer, _bcast_buffer, _vectorSize);
  Mate_LocalBarrier();

  return MPI_SUCCESS;
}

extern "C" void mate_bcast_( void *buffer, int *count, MPI_Datatype *datatype, int *root, MPI_Comm *comm) {  Mate_Bcast(buffer, *count, *datatype, *root, *comm); }
