#include "mate_runtime.h"
#include "mate_comm.h"
#include "mate_rank.h"
#include <mpi.h>

using namespace Mate;

// Mate_Init replaces MPI_Init.
int Mate_Init(int* argc, char*** argv)
{
	// All of the functions of MPI_Init have been satisfied before during the
	// initialization of Mate. However, we need to update the arguments by
	// the ones given by MPI_Init at that moment into the task right now.
	if (*argc == 0 || *argv == 0) return 0;
	copyArguments(argc, argv, _runtime->_argc, _runtime->_argv);
	return 0;
}

extern "C" void mate_init_(int* argc, char*** argv) { Mate_Init(argc, argv); }
