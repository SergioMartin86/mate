#include "mate_runtime.h"
#include "mate_comm.h"
#include "mate_rank.h"
#include <mpi.h>

using namespace Mate;


int Mate_Comm_split(MPI_Comm comm, int color, int key, MPI_Comm *newcomm)
{
	Mate_Rank* currentTask = getCurrentRank();

	Mate_LocalBarrier();

	if (currentTask->_localId == 0)
	{
	 _MPILock.lock();
  	int ret = MPI_Comm_split(comm, color, key, newcomm);
	  if (ret != MPI_SUCCESS) {fprintf(stderr, "[MATE] Error: MPI_Comm_split Failed\n"); MPI_Abort(MPI_COMM_WORLD, -1);}
	 _MPILock.unlock();
	}

	Mate_LocalBarrier();

	return MPI_SUCCESS;
}
