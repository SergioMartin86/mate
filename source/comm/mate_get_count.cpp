#include "mate_runtime.h"
#include "mate_comm.h"
#include "mate_rank.h"
#include <mpi.h>

using namespace Mate;


int Mate_Get_count( const MPI_Status *status, MPI_Datatype datatype, int *count )
{
	_MPILock.lock();
	int ret = MPI_Get_count(status, datatype, count);
	_MPILock.unlock();
	if (ret != MPI_SUCCESS) {fprintf(stderr, "[MATE] Error: Mate_Get_count Failed\n"); MPI_Abort(MPI_COMM_WORLD, -1);}

	return ret;
}
