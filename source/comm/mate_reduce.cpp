#include "mate_runtime.h"
#include "mate_comm.h"
#include "mate_rank.h"
#include <cstring>
#include <mpi.h>

using namespace Mate;

bool _initializedReduce;
unsigned long int _reduce_accumulator_size;
void* _reduce_accumulator;

// Mate_Reduce replaces Mate_Reduce. Reduce in Mate works in two stages:
// 1) A lineal complexity Node-local reduction is made among Mate tasks belonging to the same MPI Process.
// 2) A call to the actual MPI_Reduce is made with the values obtained before.
int Mate_Reduce(const void *sendbuf, void *recvbuf, int count, MPI_Datatype datatype, MPI_Op op, int root, MPI_Comm comm)
{
	Mate_Rank* currentTask = getCurrentRank();

	if (comm == MPI_COMM_WORLD)
	{
		Mate_Barrier(MPI_COMM_WORLD);

		int _localRoot;
		if (Mate_isLocalProcessRank(root, currentTask->_globalId)) _localRoot = _runtime->_globalToLocalRankIdMap[root];
		else																				 	             _localRoot = 0;

		if (currentTask->_localId == _localRoot)
			{
				unsigned long int _vectorSize = Mate_Get_Size(count, datatype);
				_reduce_accumulator = (void*) malloc (_vectorSize);
				memcpy(_reduce_accumulator, sendbuf, _vectorSize);
			}

		Mate_LocalBarrier();
		if (currentTask->_localId != _localRoot)
		{
			_runtime->reduceLock.lock();
			void* redbuf = _reduce_accumulator;
			Mate_OpExec(redbuf, sendbuf, count, datatype, op);
			_runtime->reduceLock.unlock();

		}
		Mate_LocalBarrier();

		if (currentTask->_localId == _localRoot)
		{
			// Determining receiving MPI process ID using the global task map
			int MPI_root = _runtime->_global_rank_map[root];
			_MPILock.lock();
			int ret = MPI_Reduce(_reduce_accumulator, recvbuf, count, datatype, op, MPI_root, comm);
			_MPILock.unlock();
			if (ret != MPI_SUCCESS) {fprintf(stderr, "[MATE] Error: MPI_Reduce Failed\n"); MPI_Abort(MPI_COMM_WORLD, -1);}
			free (_reduce_accumulator);
		}

		Mate_Barrier(MPI_COMM_WORLD);
	}
	else
	{
		Mate_LocalBarrier();

		if (currentTask->_localId == 0)
		{
			_MPILock.lock();
			int ret = MPI_Reduce(sendbuf, recvbuf, count, datatype, op, root, comm);
			_MPILock.unlock();
		}

		Mate_LocalBarrier();
	}

  return 0;
}

extern "C" int mate_reduce_(const void *sendbuf, void *recvbuf, int* count, MPI_Datatype* datatype, MPI_Op* op, int* root, MPI_Comm* comm) { return Mate_Reduce(sendbuf, recvbuf, *count, *datatype, *op, *root, *comm); }
