#include "mate_runtime.h"
#include "mate_comm.h"
#include "mate_rank.h"
#include <mpi.h>

using namespace Mate;

// Mate_Abort replaces MPI_Abort.
int Mate_Abort(MPI_Comm comm, int errorcode)
{
	return MPI_Abort(comm, errorcode);
}

extern "C" int mate_abort_(MPI_Comm* comm, int* errorcode) { return Mate_Abort(*comm, *errorcode); }
