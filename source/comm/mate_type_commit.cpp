#include "mate_runtime.h"
#include "mate_comm.h"
#include "mate_rank.h"
#include <mpi.h>

using namespace Mate;


int Mate_Type_commit(MPI_Datatype* type)
{
	_MPILock.lock();
	int ret = MPI_Type_commit(type);
	_MPILock.unlock();
	if (ret != MPI_SUCCESS) {fprintf(stderr, "[MATE] Error: MPI_Type_commit Failed\n"); MPI_Abort(MPI_COMM_WORLD, -1);}
	return ret;
}
