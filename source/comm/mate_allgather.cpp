#include "mate_runtime.h"
#include "mate_comm.h"
#include "mate_rank.h"
#include <cstring>
#include <mpi.h>

using namespace Mate;

void* _Allgather_sendbuffer;
void* _Allgather_recvbuffer;
unsigned long int _Allgather_sendbuffer_size;

int Mate_Allgather(const void *sendbuf, int sendcount, MPI_Datatype sendtype, void *recvbuf, int recvcount, MPI_Datatype recvtype, MPI_Comm comm)
{
	Mate_Rank* currentTask = getCurrentRank();
  int localTaskCount  = _runtime->_local_rank_count;
  int globalTaskCount = _runtime->_global_rank_count;
  int taskSendMsgSize = Mate_Get_Size(sendcount, sendtype);
  int taskRecvMsgSize = Mate_Get_Size(recvcount, recvtype);
	unsigned long int _sendVectorSize = taskSendMsgSize*localTaskCount;
	unsigned long int _recvVectorSize = taskRecvMsgSize*globalTaskCount;

//	if (currentTask->_localId == 0)printf("Allgather\n");
  int _localRoot = 0;

  if (_Allgather_sendbuffer_size < _sendVectorSize)
  {
  	unsigned int newVectorSize = _sendVectorSize*4;
		if (currentTask->_localId == _localRoot)
		{
			free(_Allgather_sendbuffer);
			_Allgather_sendbuffer = (void*) malloc (newVectorSize);
		}
	  Mate_LocalBarrier();
	  if (currentTask->_localId == _localRoot) _Allgather_sendbuffer_size = newVectorSize;
  }

  memcpy((char*)_Allgather_sendbuffer + taskSendMsgSize*currentTask->_localId, sendbuf, taskSendMsgSize);
  Mate_LocalBarrier();

  if (currentTask->_localId == _localRoot)
  {
  	free(_Allgather_recvbuffer);
  	_Allgather_recvbuffer = (void*) malloc (_recvVectorSize);

  	int startTaskId = 0;
  	int* recvCounts  = (int*) calloc (sizeof(int), _runtime->_process_count);
  	int* displCounts = (int*) calloc (sizeof(int), _runtime->_process_count);
  	for (int i = 0; i < _runtime->_process_count; i++)
  	{
  		recvCounts[i] = recvcount*_runtime->_global_rank_count_per_process[i];
  		displCounts[i] = startTaskId;
  		startTaskId += recvCounts[i];
  	}

  	_MPILock.lock();
	  int ret = MPI_Allgatherv(_Allgather_sendbuffer, sendcount*localTaskCount, sendtype, _Allgather_recvbuffer, recvCounts, displCounts, recvtype, MPI_COMM_WORLD);
		if (ret != MPI_SUCCESS) {fprintf(stderr, "[MATE] Error: MPI_Allgather Failed\n"); MPI_Abort(MPI_COMM_WORLD, -1);}
	  _MPILock.unlock();

	  free(recvCounts);
	  free(displCounts);
  }

  Mate_LocalBarrier();
  memcpy(recvbuf, _Allgather_recvbuffer, _recvVectorSize);

  return 0;
}

extern "C" int mate_allgather_(const void *sendbuf, int* sendcount, MPI_Datatype* sendtype, void *recvbuf, int* recvcount, MPI_Datatype* recvtype, MPI_Comm* comm) { return Mate_Allgather(sendbuf, *sendcount, *sendtype, recvbuf, *recvcount, *recvtype, *comm); }
