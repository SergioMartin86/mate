#include "mate_runtime.h"
#include "mate_comm.h"
#include "mate_rank.h"
#include "mate_aux.h"
#include <mpi.h>

using namespace Mate;

Lock timeLock;
bool firstLocalTimeTaken;
double firstLocalTime;
double lastLocalTime;

// Mate_Wtime replaces MPI_Wtime.
double Mate_Wtime()
{
	return MPI_Wtime();
}

extern "C" void mate_wtime_(double* time)
{
   *time = MPI_Wtime();
}

void Mate_SetFirstLocalTime()
{
  timeLock.lock();
  if (firstLocalTimeTaken == false)
  {
    firstLocalTime = Mate_Wtime();
    firstLocalTimeTaken = true;
  }
  timeLock.unlock();
}

void Mate_SetLastLocalTime()
{
  timeLock.lock();
    lastLocalTime = Mate_Wtime();
  timeLock.unlock();
}

double Mate_GetProcessTime()
{
  return lastLocalTime - firstLocalTime;
}
