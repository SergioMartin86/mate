#include "mate_runtime.h"
#include "mate_comm.h"
#include "mate_rank.h"
#include <cstring>
#include <mpi.h>

using namespace Mate;

void* _Allgatherv_sendbuffer;
void* _Allgatherv_recvbuffer;
unsigned long int _Allgatherv_sendbuffer_size;
unsigned long int _AllgathervRecvVectorSize;
unsigned long int _AllgathervSendVectorSize;
vector<int> _newAllgathervRecvCounts;
vector<int> _newAllgathervDispCounts;

int Mate_Allgatherv(const void *sendbuf, int sendcount, MPI_Datatype sendtype, void *recvbuf, const int *recvcounts, const int *displs, MPI_Datatype recvtype, MPI_Comm comm)
{
	Mate_Rank* currentTask = getCurrentRank();
  int localTaskCount  = _runtime->_local_rank_count;
	int __recvTypeSize;
	int __sendTypeSize;
	Mate_Type_size(recvtype, &__recvTypeSize);
	Mate_Type_size(sendtype, &__sendTypeSize);

  int _localRoot = 0;
	int startTaskId = 0;
	int endTaskId = 0;
//	if (currentTask->_localId == 0)printf("Allgatherv\n");
	for (int i = 0; i < _runtime->_process_count; i++)
	{
		startTaskId = _runtime->_local_rank_count*i;
		endTaskId = (i+1)*_runtime->_local_rank_count-1;
		_newAllgathervDispCounts[i] = displs[startTaskId];
		_newAllgathervRecvCounts[i] = displs[endTaskId] + recvcounts[endTaskId] - _newAllgathervDispCounts[i];
	}

	unsigned long int _recvVectorSize = __sendTypeSize*(displs[endTaskId] + recvcounts[endTaskId]);
	unsigned long int _sendVectorSize = __sendTypeSize*_newAllgathervRecvCounts[_runtime->_process_id];
  int myDispl = displs[currentTask->_globalId] - _newAllgathervDispCounts[_runtime->_process_id];

  if (_Allgatherv_sendbuffer_size < _sendVectorSize)
  {
  	 _sendVectorSize = _sendVectorSize*4;
		if (currentTask->_localId == _localRoot)
			{
			  free(_Allgatherv_sendbuffer);
			  _Allgatherv_sendbuffer = (void*) malloc (_sendVectorSize);
			}
	  Mate_LocalBarrier();
	  if (currentTask->_localId == _localRoot) _Allgatherv_sendbuffer_size = _sendVectorSize;
  }

  memcpy((char*)_Allgatherv_sendbuffer + myDispl*__sendTypeSize, sendbuf, sendcount*__sendTypeSize);
  Mate_LocalBarrier();

  if (currentTask->_localId == _localRoot)
  {
  	free(_Allgatherv_recvbuffer);
		_Allgatherv_recvbuffer = (void*) malloc (_recvVectorSize);

		_MPILock.lock();
	   int ret = MPI_Allgatherv(_Allgatherv_sendbuffer, _newAllgathervRecvCounts[_runtime->_process_id], sendtype, _Allgatherv_recvbuffer, _newAllgathervRecvCounts.data(), _newAllgathervDispCounts.data(), recvtype, MPI_COMM_WORLD);
	 _MPILock.unlock();
	  if (ret != MPI_SUCCESS) {fprintf(stderr, "[MATE] Error: MPI_Allgather Failed\n"); MPI_Abort(MPI_COMM_WORLD, -1);}
  }

  Mate_LocalBarrier();
   memcpy(recvbuf, _Allgatherv_recvbuffer, _recvVectorSize);
//  Mate_LocalBarrier();

  return 0;
}
