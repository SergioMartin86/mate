#include "mate_runtime.h"
#include "mate_comm.h"
#include "mate_rank.h"
#include "mate_mpi.h"
#include <cstring>
#include <mpi.h>

using namespace Mate;

int Mate_Send(const void* buffer, int count, MPI_Datatype datatype, int destProcess, int destRank, int tag, MPI_Comm comm, MPI_Request* request)
{
	Mate_Rank* currentTask = getCurrentRank();
	Mate_Region* curRegion = currentTask->_regionMap[currentTask->_currentRegionId];

	if (tag > _mate_max_tag-1)
	{
		fprintf(stderr, "[MATE] Irecv Error: Invalid tag (%d).\n", tag);
		fprintf(stderr, "[MATE] Tag should be a value between 0 and %d.\n", _mate_max_tag-1);
		MPI_Abort(MPI_COMM_WORLD, -1);
	}

	int sourceRank = currentTask->_localId;

	// Determining mateTag value to use as tag
	int mateTag =  (tag << MAXLOCALTASKS*2) + (sourceRank << MAXLOCALTASKS) + destRank;

	_MPILock.lock();
	MPI_Isend(buffer, count, datatype, destProcess, mateTag, MPI_COMM_WORLD, request);
	_MPILock.unlock();

	curRegion->_MPIRequests.push_back(*request);

 return MPI_SUCCESS;
}

extern "C" int mate_send_(void* buffer, int* count, MPI_Datatype* datatype, int* destProcess, int* destRank, int* tag, MPI_Comm* comm, MPI_Request* request) { return Mate_Send(buffer, *count, *datatype, *destProcess, *destRank, *tag, *comm, request); }
