#include "mate_runtime.h"
#include "mate_comm.h"
#include "mate_rank.h"
#include <mpi.h>

using namespace Mate;


int Mate_Address(void *location, MPI_Aint *address)
{
	_MPILock.lock();
	int ret = MPI_Address(location, address);
	_MPILock.unlock();
	if (ret != MPI_SUCCESS) {fprintf(stderr, "[MATE] Error: MPI_Address Failed\n"); MPI_Abort(MPI_COMM_WORLD, -1);}

	return ret;
}
