#include "mate_runtime.h"
#include "mate_comm.h"
#include "mate_rank.h"
#include <mpi.h>

using namespace Mate;


// Mate_Finalize replaces MPI_Finalize.
int Mate_Finalize()
{
  return MPI_SUCCESS;
}


extern "C" int mate_finalize_() { return Mate_Finalize(); }
