#include "mate_runtime.h"
#include "mate_comm.h"
#include "mate_rank.h"
#include <mpi.h>

using namespace Mate;


int Mate_Type_size(MPI_Datatype type, int* size)
{
	_MPILock.lock();
	int ret = MPI_Type_size(type, size);
	_MPILock.unlock();
	if (ret != MPI_SUCCESS) {fprintf(stderr, "[MATE] Error: MPI_Type_size Failed\n"); MPI_Abort(MPI_COMM_WORLD, -1);}
	return ret;
}
