// Copyright (c) 2015, The Regents of the University of California.
// Produced at the University of California, San Diego
// by Sergio Martin and Scott B. Baden
// All rights reserved.
// 
// This file is part of Mate. For details, see http://mate.ucsd.edu/

// Mate includes
#include <unistd.h>
#include <limits.h>
#include "hwloc.h"
#include "mate_runtime.h"
#include "string.h"
#include <stdlib.h>
#include <sstream>
#include <map>

namespace Mate
{

const int __threadLevel = MPI_THREAD_SINGLE;

MateRuntime*  _runtime;

int MateRuntime::initializeMate(int argc, char** argv)
{
  int provided;
  MPI_Init_thread(&argc, &argv, __threadLevel, &provided);
  if (provided != __threadLevel) {printf("[MATE] MPI Init Error. Level required: %d, Level provided: %d\n", __threadLevel, provided); exit(-1);}

  MPI_Comm_size(MPI_COMM_WORLD,&_process_count);
  MPI_Comm_rank(MPI_COMM_WORLD,&_process_id);

  // Store main() arguments
  this->_argc = argc;
  this->_argv = argv;

  // Processing MATE arguments
  initializeArguments();

  // Initializing Tags for the MPI backend
  initializeMateTags();

  //printf("[MATE] Mapping MATE workers and tasks:\n");
  initializeWorkerMapping();
  initializeTaskMapping();

  // Generating structures for collective operations
  for (int i = 0; i < _runtime->_process_count; i++)
  {
    _newAllgathervRecvCounts.push_back(0);
    _newAllgathervDispCounts.push_back(0);
  }

	// Initializing Mate_Rank coroutines using a trigger function (like the ones used to initialize threads). Since they are scheduled by the process,
	// they don't start right away, they start as soon as we call the resume() function.
	for (int i = 0; i < _local_rank_count; i++)
	{
		_currentRank = &_ranks[i];
		_ranks[i]._rankContext = co_create(32768*sizeof(void*), Mate::__taskWrapper);
		_ranks[i].resume();
	}

  // Set barrier for local threads and the runtime
	initBarrier = new Barrier(_local_worker_count+1);

  // Creating worker threads. As soon as they are created, they start running because they are scheduled by the OS.
  //printf("[MATE] Creating %d Threads.\n", _local_worker_count);
  for(int i = 0; i < _local_worker_count; i++)
  {
  	int ret = pthread_create(&(_workers[i]._thread), 0, (void*(*)(void*))__run, &_workers[i]);
    if (ret != 0) { if (_process_id == 0) printf("[MATE] Error creating thread for worker %d.\n", i); exit(0);}
  	_threadIdToWorkerMap[_workers[i]._thread] = &_workers[i];
  }

  // Waiting for all threads to initialize
	initBarrier->wait();
	initBarrier->wait();

  // Wait for threads threads to end
	for(int i = 0; i < _local_worker_count; i++)
		pthread_join(_workers[i]._thread, NULL);

	return MPI_Finalize();
}

MateRuntime::MateRuntime()
{
	// Setting initial values
	_reduce_accumulator  = malloc(MATE_INITIAL_BUFFER_ALLOCATION_SIZE);
	_local_allreduce_accumulator  = malloc(MATE_INITIAL_BUFFER_ALLOCATION_SIZE);
	_useMateTiming = false;
	_isGlobalBarrier = false;

	_global_rank_count = 0;
	_local_worker_count = 0;
	_local_rank_finished = 0;

  _Allgather_sendbuffer_size = MATE_INITIAL_BUFFER_ALLOCATION_SIZE;
	_Allgatherv_sendbuffer_size = MATE_INITIAL_BUFFER_ALLOCATION_SIZE;
	_Allgather_sendbuffer = malloc(MATE_INITIAL_BUFFER_ALLOCATION_SIZE);
	_Allgatherv_sendbuffer = malloc(MATE_INITIAL_BUFFER_ALLOCATION_SIZE);
	_Allgather_recvbuffer = malloc(MATE_INITIAL_BUFFER_ALLOCATION_SIZE);
	_Allgatherv_recvbuffer = malloc(MATE_INITIAL_BUFFER_ALLOCATION_SIZE);
	_gather_buffer = malloc(MATE_INITIAL_BUFFER_ALLOCATION_SIZE);
	_AllgathervRecvVectorSize = 0;
	_AllgathervSendVectorSize = 0;

	_reduce_accumulator_size = MATE_INITIAL_BUFFER_ALLOCATION_SIZE;
	_reduce_accumulator = malloc(MATE_INITIAL_BUFFER_ALLOCATION_SIZE);
	_allreduce_accumulator = malloc(MATE_INITIAL_BUFFER_ALLOCATION_SIZE);
	_initializedReduce = true;

	firstLocalTimeTaken = false;
	firstLocalTime = 0;
	lastLocalTime = 0;
}


void MateRuntime::initializeArguments()
{
	 // Just as MPI_Init does, we first need to read and get rid of Mate's extra arguments
	 int nonMateArgc = 0;
	 bool tasksDefined = false;

	 // We start analyzing arguments, in search for mate specific arguments.
	 for (int i = 0; i < this->_argc; i++)
	 {

   bool isMateArgument = false;

   if(!strcmp(this->_argv[i], "--mate-ranks"))
   {
    i++;
    if (i < this->_argc) _global_rank_count = atoi(this->_argv[i]);
    tasksDefined = true;
    isMateArgument = true;
   }

    // It this is a mate argument, we ignore it and don't pass it to the user's program arguments. This way, mate is completely transparent.
   if (isMateArgument == false)
   {
    // If this it not a mate argument, we keep and pass it to the user's program.
    // If indexes don't match, we need to make a hard copy.
    if (i != nonMateArgc)
    {
      int argSize = strlen(this->_argv[i]);

     this->_argv[nonMateArgc] = (char*) malloc(argSize*sizeof(char));
     if (this->_argv[nonMateArgc] == NULL) { if (_process_id == 0) printf("[MATE] Error: Memory Allocation Failure.\n"); exit(0);}

     strcpy(this->_argv[nonMateArgc], this->_argv[i]);
    }
    nonMateArgc++;
   }
	 }

	 // We update this->_argc with the count of non-Mate arguments.
	 this->_argc = nonMateArgc;
	 if (tasksDefined == false) { if (_process_id == 0) printf("[MATE] Error: --mate-tasks argument not defined.\n"); exit(0);}

		// Calculating Local Mate_Rank Count
		_local_rank_count = _global_rank_count / _process_count;
	  if (_global_rank_count % _process_count > _runtime->_process_id) _local_rank_count++;
}


void MateRuntime::initializeWorkerMapping()
{
 std::vector<std::vector<int>> coreTopology;

 hwloc_topology_t topology;
 hwloc_topology_init(&topology);
 hwloc_topology_load(topology);
 int tdepth = hwloc_topology_get_depth(topology);

 for (int depth = 0; depth < tdepth; depth++)
 {
  for (int i = 0; i < hwloc_get_nbobjs_by_depth(topology, depth); i++)
  {
   char typeString[128];
   hwloc_obj_t currentObj = hwloc_get_obj_by_depth(topology, depth, i);
   hwloc_obj_type_snprintf(typeString, sizeof(typeString), currentObj, 0);
   if (strcmp(typeString, "Core") == 0)
   {
    std::vector<int> currentTopology;
    //printf("Core %u\n", currentObj->os_index);
    //printf("---------\n");
    for (int j = 0; j < currentObj->arity; j++)
    {
     hwloc_obj_t childObj = currentObj->children[j];
     hwloc_obj_type_snprintf(typeString, sizeof(typeString), childObj, 0);
     if (strcmp(typeString, "PU") == 0)
     {
       //printf("PU: %u\n", childObj->os_index);
       currentTopology.push_back(childObj->os_index);
     }
    }
    //printf("\n");
    coreTopology.push_back(currentTopology);
   }
  }
 }

 char hostname[HOST_NAME_MAX];
 gethostname(hostname, HOST_NAME_MAX);

 // Creating storage for Worker count for each MATE process
 char _global_hostnames[_process_count][HOST_NAME_MAX];

 // Obtaining exact amount of threads per each MATE process (they could differ, but normally won't, if using a homogeneous cluster)
 //printf("[MATE] Gathering MATE tasks and worker information...\n");
 MPI_Allgather(&hostname, HOST_NAME_MAX, MPI_CHAR, _global_hostnames, HOST_NAME_MAX, MPI_CHAR, MPI_COMM_WORLD);

 std::vector<int> _nodeLocalTeam;
 for (size_t i = 0; i < _process_count; i++)
  if (strcmp(_global_hostnames[i], hostname) == 0)
   _nodeLocalTeam.push_back(i);

 size_t coreCount = coreTopology.size();
 size_t localProcessCount = _nodeLocalTeam.size();
 _local_worker_count = coreCount / localProcessCount;

 int processLocalId;
 for (size_t i = 0; i < _nodeLocalTeam.size(); i++)
   if (_nodeLocalTeam[i] == _process_id) processLocalId = i;

 if (coreCount % localProcessCount != 0)
 {
  if(_process_id == 0) printf("[MATE] Error: The number of MPI processes per node %lu is not a divisor of the node core count %lu.\n", localProcessCount, coreCount);
  exit(0);
 }

// if(_process_id == 0)
// {
//  for (size_t i = 0; i < coreTopology.size(); i++)
//  {
//   printf("Core %d: ", i);
//   for (size_t j = 0; j < coreTopology[i].size(); j++)
//    printf(" %d ", coreTopology[i][j]);
//   printf("\n");
//  }
//
//  for (size_t i = 0; i < _process_count; i++) printf("Host %d: %s\n", i, _global_hostnames[i]);
//
//  printf("Process Local Team: ");
//  for (size_t i = 0; i < _nodeLocalTeam.size(); i++) printf(" %d", i);
//  printf("\n");
// }

 // Creating local Threads
 _workers = (Worker*) calloc (sizeof(Worker), _local_worker_count);
 for (int i = 0; i < _local_worker_count; i++) _workers[i].initialize(i, this);

 int startCore = _local_worker_count * processLocalId;
 for (int i = 0; i < _local_worker_count; i++)
  for (int j = 0; j < coreTopology[startCore + i].size(); j++)
   _workers[i]._affinity.push_back(coreTopology[startCore + i][j]);

 // Creating storage for Worker count for each MATE process
 auto _global_worker_count_per_process = new int[_process_count];

 // Obtaining exact amount of threads per each MATE process (they could differ, but normally won't, if using a homogeneous cluster)
 //printf("[MATE] Gathering MATE tasks and worker information...\n");
 MPI_Allgather(&_local_worker_count, 1, MPI_INT, _global_worker_count_per_process, 1, MPI_INT, MPI_COMM_WORLD);

 // Calculating and verifying total global task count
 //printf("[MATE] Calculating Total Mate_Rank Counts...\n");
 int _global_rank_count_check = 0;
 _global_worker_count = 0;
 for (int i = 0; i < _process_count; i++)  _global_worker_count += _global_worker_count_per_process[i];
}

void MateRuntime::initializeTaskMapping()
{
  // Creating local Tasks
  _ranks = (Mate_Rank*) calloc (sizeof(Mate_Rank), _local_rank_count);
  for(int i = 0; i < _local_rank_count; i++)  _ranks[i].initialize(i);
  _ranksLock = new Lock[_local_rank_count];

  // Creating storage for Worker and Mate_Rank count for each MATE process
  _global_rank_count_per_process = new int[_process_count];

  // Obtaining exact amount of tasks and threads per each MATE process (they could differ, but normally won't, if using a homogeneous cluster)
  //printf("[MATE] Gathering MATE tasks and worker information...\n");
  MPI_Allgather(&_local_rank_count,   1, MPI_INT, _global_rank_count_per_process,   1, MPI_INT, MPI_COMM_WORLD);

  // Calculating and verifying total global task count
  //printf("[MATE] Calculating Total Mate_Rank Counts...\n");
  int _global_rank_count_check = 0;
  for (int i = 0; i < _process_count; i++)   _global_rank_count_check   += _global_rank_count_per_process[i];

  if (_global_rank_count_check != _global_rank_count)
    {printf("[MATE] Internal Error: Specified task count and computed task count do not match.\n"); exit(-1);}

  // Allocating storage for globalToLocal Maps
  _globalToLocalRankIdMap = new int[_global_rank_count];


	// Determining which is the starting global task mapping
  int taskGlobalId = 0;
	for (int i = 0; i < _process_count; i++)
  	for (int j = 0; j < _global_rank_count_per_process[i]; j++)
  	{
  		if (i == _process_id) _ranks[j]._globalId = taskGlobalId;
  		_globalToLocalRankIdMap[taskGlobalId] = j;
  		taskGlobalId++;
  	}

	// For each (global) task, determine MPI process ID and save it into a map for easy access later
	_global_rank_map = new int[_global_rank_count];
	int currentTask = 0;
	for (int i = 0; i < _process_count; i++)
		for (int j = 0; j < _global_rank_count_per_process[i]; j++)
			_global_rank_map[currentTask++] = i;

}

} // namespace Mate


