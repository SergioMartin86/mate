// Copyright (c) 2015, The Regents of the University of California.
// Produced at the University of California, San Diego
// by Sergio Martin and Scott B. Baden
// All rights reserved.
// 
// This file is part of Mate. For details, see http://mate.ucsd.edu/

#ifndef MATE_RANK_H_
#define MATE_RANK_H_

#include "mate_runtime.h"
#include "mate_comm.h"
#include "mate_aux.h"
#include "mpi.h"

#include <map>
#include <stack>
#include <vector>

using namespace std;

#define __ROOT 0
#define __EVAL -1
#define __MATE_NO_REGION -2

#define __MATE_NORMAL_GRAPH 0
#define __MATE_FOR_GRAPH 1

namespace Mate
{
class MateRuntime;

struct mateRegionStruct;
struct mateDependencyStruct;

typedef struct mateGraphStruct {
  vector<mateRegionStruct*> _regionVector;
  int _activeRegions;
  bool _enforceMaxStep;
  int _maxStep;
  int _graphType;
} Mate_Graph;

typedef struct mateDependencyStruct {
	mateRegionStruct* _depRegion;
	int _depDelay;
} Mate_Dependency;

typedef struct mateRegionStruct {
	size_t _regionId;
	int _currentStep;
	bool _active;
	vector<MPI_Request> _MPIRequests;
	vector<Mate_Dependency*> _deps;
} Mate_Region;


class Mate_Rank
{
public:

	// Mate_Rank variables
    int _globalId;
    int _localId;

    bool _isReady;
    bool _isBarrier;

    int _argc;
    char** _argv;
    bool _isFinished;

    // Mate Regions vector
    vector<Mate_Graph*> _graphVector;
    map<size_t, Mate_Graph*> _graphMap;
    map<size_t, Mate_Region*> _regionMap;
    vector<int> _localNeighbors;

    // Profiling variables
    double _scheduleTime;
    double _barrierTime;

    // The identifier of the current region being executed
    Mate_Graph* _currentGraph;
    size_t _currentRegionId;

    /**
     * @brief User-Level thread (coroutine) containing the CPU execution state of the current rank and worker thread.
     */
    cothread_t _rankContext;
    cothread_t _yieldContext;

    // Mate_Rank constructor/Destructor
    Mate_Rank(){};

    // Mate_Rank execution functions
    void runTask();
    void execute(int argc, char** argv);
    void initialize(int localId);
    void resume();
    void yield();
    size_t getNextRegionID();

    // Helper Functions
    bool isLocalTask(int dest);
    bool isReady();
};

void __taskWrapper();

} // namespace Mate

void Mate_AddRegion(size_t regionId);
size_t Mate_EnableRegions(int n, size_t regionId[]);
bool Mate_RegisterGraph(size_t graphId, int graphType);
void Mate_InterRank(size_t srcRegionId, size_t depRegionId, int delay);
void Mate_AddDependency(size_t srcRegionId, size_t depRegionId, int delay);
void Mate_AddLocalNeighbor(int localRankId);
void Mate_EndForLoop();
size_t Mate_GetNextRegionID();

#endif // MATE_RANK_H_
