// Copyright (c) 2015-2016, The Regents of the University of California.
// Produced at the University of California, San Diego
// by Sergio Martin and Scott B. Baden
// All rights reserved.
// 
// This file is part of Mate. For details, see http://mate.ucsd.edu/

#ifndef MATE_AUX_H_
#define MATE_AUX_H_

#include <pthread.h>
#include <vector>
#include <map>
#include <set>

extern map <int, void*> idxToSHMPointerMap;

namespace Mate
{
class Mate_Rank;
class Worker;

	class Lock
	{
		pthread_mutex_t _lock;
	  public:
		Lock();
		~Lock();
		void lock();
		void unlock();
		bool trylock();
	};

	class Barrier
	{
		pthread_barrier_t _barrier;
		public:
		Barrier(int count);
		~Barrier();
		void wait();
	};

void printAffinity();
void Mate_setAffinity(vector<int> cpuIds);
Mate_Rank* getCurrentRank();
Worker* getCurrentWorker();
inline unsigned long int min(unsigned long int a, unsigned long int b) { return (a < b) ? a : b; };
bool isRootRegion(Mate_Rank* currentTask);

} // namespace Mate

void Mate_ExitWithError(int code, const char* reason);
int isLocalRoot();
void copyArguments(int* destArgc, char*** destArgv, int sourceArgc, char** sourceArgv);

void Mate_local_rank_id(int* localRankId);
void Mate_local_rank_count(int* localRankCount);
void Mate_global_process_id(int* globalProcessId);
void Mate_global_process_count(int *globalProcessCount);
void Mate_local_worker_count(int* localThreadCount);
void Mate_global_worker_count(int* globalThreadCount);

double Mate_GetTaskScheduleTime();
double Mate_GetTaskBarrierTime();
vector<double>* Mate_GetLocalThreadTimeLapses(int threadId);
vector<int>* Mate_GetLocalThreadTimeTaskIds(int threadId);
void Mate_StartTimers();
void Mate_ResetTimers();
void Mate_StopTimers();
void Mate_GetGlobalThreadScheduleTime(double* scheduleTimes);
void Mate_GetGlobalThreadComputeTime(double* computeTimes);

bool Mate_isLocalProcessRank(int dest, int current);
int Mate_getLocalRankId(int dest);
void* Mate_GetLocalPointer(int key);

extern void* localShareBuffer;
void Mate_LocalBcast(void* ptr, size_t size, int local_root);
void Mate_LocalLock();
void Mate_LocalUnlock();
void Mate_AtomicAddDouble(double* ptr, double value);


int Mate_sched_setaffinity(pid_t pid, size_t cpusetsize, const cpu_set_t *mask);
int Mate_pthread_setaffinity_np(pthread_t thread, size_t cpusetsize, const cpu_set_t *cpuset);
int Mate_pthread_setaffinity(pthread_t thread, size_t cpusetsize, const cpu_set_t *cpuset);

void Mate_CustomMapping(int* newMap);

#endif // MATE_AUX_H_
