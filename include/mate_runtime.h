// Copyright (c) 2015, The Regents of the University of California.
// Produced at the University of California, San Diego
// by Sergio Martin and Scott B. Baden
// All rights reserved.
// 
// This file is part of Mate. For details, see http://mate.ucsd.edu/

#ifndef MATE_RUNTIME_H_
#define MATE_RUNTIME_H_

#include "libco.h"
#include "mate_comm.h"
#include "mate_rank.h"
#include "mate_worker.h"
#include "mate_aux.h"
#include "mate_mpi.h"
#include <vector>
#include <queue>

using namespace std;

namespace Mate
{

class Mate_Rank;

class MateRuntime
{
public:

 Worker* _workers;
 Lock* _ranksLock;
 Mate_Rank* _ranks;
 Mate_Rank* _currentRank; // Necessary for coroutine instantiation

 int _local_rank_count;
 int _local_rank_finished;
 int _local_worker_count;

	vector< vector<int> > _affinity;

	bool _useMateTiming;

	int    _argc;
	char** _argv;

	Lock localLock;
	Lock runtimeLock;
	Lock switchLock;
	Lock reduceLock;
	Lock barrierLock;
	Lock shmLock;
	Lock commLock;
	Barrier* initBarrier;

	int _process_id;
	int _process_count;
	int _processes_per_node;

	int* _global_rank_count_per_process;
	int  _global_rank_count;
	int* _global_rank_map;

 int  _global_worker_count;
	int* _globalToLocalRankIdMap;

 std::map<pthread_t, Worker*> _threadIdToWorkerMap;

  vector<int> _CPUIdVector;

	int initializeMate(int argc, char** argv);
	void initializeArguments();
	void initializeTaskMapping();
	void initializeWorkerMapping();

	~MateRuntime() {};
	MateRuntime();
};

extern MateRuntime*  _runtime;

} // namespace Mate

#endif // MATE_RUNTIME_H_
