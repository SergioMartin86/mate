// Copyright (c) 2015, The Regents of the University of California.
// Produced at the University of California, San Diego
// by Sergio Martin and Scott B. Baden
// All rights reserved.
// 
// This file is part of Mate. For details, see http://mate.ucsd.edu/

#ifndef MATE_MPI_H_
#define MATE_MPI_H_

#define MAXLOCALTASKS 8

#include "mpi.h"
#include "mate_comm.h"
#include "mate_aux.h"
#include "mate_rank.h"
#include <vector>

using namespace std;
using namespace Mate;

extern int _mate_max_tag;
extern int _mate_max_localtasks;
extern Lock _MPILock;

void initializeMateTags();

#endif // MATE_REMOTE_H_



