# Copyright (c) 2015, The Regents of the University of California.
# Produced at the University of California, San Diego
# by Sergio Martin and Scott B. Baden
# All rights reserved.
# 
# This file is part of Mate. For details, see http://mate.ucsd.edu/

include Makefile.in

.PHONY: all
all: mate
	@echo "[MATE] compiled correctly. To install, execute: make install"

.PHONY: mate 
mate: $(CURRENT_DIR)/build/hwloc/include/hwloc.h
	@$(MAKE) -j $(BUILD_JOBS) -C source
	
.PHONY: install
install: 
	@echo "[MATE] Installing Dependencies (HWLoc)..."
	@$(MAKE) -C $(CURRENT_DIR)/build/hwloc install > /dev/null 2>&1
	@echo "[MATE] Installing MATE...                                             "
	@mkdir -p $(PREFIX)/lib
	@mkdir -p $(PREFIX)/include
	@cp source/libmate.so $(PREFIX)/lib
	@cp include/* $(PREFIX)/include -r
	@cp source/mate-translate $(PREFIX)
	@cat scripts/mate-cflags.in | sed -e "s%(CXXFLAGS)%$(CXXFLAGS)%g" > $(PREFIX)/mate-cflags
	@chmod a+x  $(PREFIX)/mate-cflags
	@cat scripts/mate-libs.in | sed -e "s%(LIBS)%$(LIBS)%g" > $(PREFIX)/mate-libs
	@chmod a+x  $(PREFIX)/mate-libs
	@cat scripts/mate-cxx.in | sed -e "s%(MPICXX)%$(MPICXX)%g" > $(PREFIX)/mate-cxx
	@chmod a+x  $(PREFIX)/mate-cxx
	@echo "[MATE] Done. To finish installation, add MATE binaries to your path:"
	@echo "[MATE]   export PATH=$(PREFIX):\$$PATH"
	
$(CURRENT_DIR)/build/hwloc/include/hwloc.h:
	@echo "[MATE] Making Dependencies (HWLoc)..."
	@mkdir -p $(CURRENT_DIR)/build/
	@rm -rf $(CURRENT_DIR)/build/hwloc
	@echo "[MATE] Cloning HWloc..." 
	@cd $(CURRENT_DIR)/build/;	git clone --single-branch --branch v2.2  https://github.com/open-mpi/hwloc.git > /dev/null 2>&1
	@echo "[MATE] Configuring HWloc..."
	@cd $(CURRENT_DIR)/build/hwloc; ./autogen.sh > /dev/null 2>&1; ./configure --prefix=${PREFIX}/hwloc > /dev/null 2>&1
	@echo "[MATE] Making HWloc..."
	@$(MAKE) -C $(CURRENT_DIR)/build/hwloc all > /dev/null 2>&1
	
.PHONY: clean
clean:
	@$(MAKE) -C source clean
